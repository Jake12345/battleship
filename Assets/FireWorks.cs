﻿using UnityEngine;
using System.Collections;

public class FireWorks : MonoBehaviour
{

    private GameObject explosion;
    public GameObject explosionPrefab;
    public GameObject explosionslocation;
    public Canvas canvas;

    // Use this for initialization
    void Start()
    {
        canvas.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

    }



    void OnTriggerEnter(Collider collision)
    {
        Debug.Log("collision");
        canvas.enabled = true;

        explosion = Instantiate(explosionPrefab, explosionslocation.transform.position, explosionslocation.transform.rotation) as GameObject;

    }
}

