﻿using UnityEngine;
using System.Collections;

public class Float : MonoBehaviour {

    public int force;
    public GameObject water;
    public Rigidbody rb;

    // Use this for initialization
    void Start()
    {

        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        

        if (transform.position.y < water.transform.position.y)
        {
            rb.AddForce(transform.up * force * 10);
        }
    }
}
