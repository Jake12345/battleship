﻿using UnityEngine;
using System.Collections;

public class ExplosionDestory : MonoBehaviour {

    public int time;

	// Use this for initialization
	void Start () 
    {
	StartCoroutine("DestroyMe");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

     IEnumerator DestroyMe()
     {
         yield return new WaitForSeconds(time);
        
          Destroy(gameObject);

          Debug.Log("explosionDestoryed");
     }

}
