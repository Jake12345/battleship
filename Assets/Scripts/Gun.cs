﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour {


    private Transform m_transformer;
    public GameObject bulletPrefab;


    // Use this for initialization
    void Start()
    {
        m_transformer = transform;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Instantiate(bulletPrefab, m_transformer.position + m_transformer.forward * 3.0f, m_transformer.rotation);
        }
    }
}
