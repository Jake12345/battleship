﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Player2 : MonoBehaviour
{

    public KeyCode left;
    public KeyCode right;
    public KeyCode BulletCamera;
    public float speed = 0.0f;
    public float turnspeed = 2.0f;
    public float reverse = 0.0f;

    public KeyCode Up;
    public KeyCode Down;
    private int gear = 2;
    private int buttonDelay = 0;
    public Button forwardGUI;
    public Button backGUI;
    public Button notMovingGUI;

    public GameObject shieldPrefab;
    public Transform Boat;
    public static bool shieldOn = false; //Static keyword makes this variable a Member of the class, not of any particular instance. As such it can now be taken from this scrip and be used in another
    private GameObject shield;
    public static bool shieldHit = false;

    public GameObject onFirePrefab;
    private GameObject onFire;
    private static bool fireOn;
    private int fireOnOff = 0;

    public GameObject smokeOnPrefab;
    private GameObject smokeOn;
    private static bool onSmoke;
    private int smokeOnOff = 0;

    public static bool nukeActive;
    private bool nukeOnline = false;
    
    


    // Use this for initialization
    void Start()
    {
        shield = Instantiate(shieldPrefab, Boat.position, Boat.transform.rotation) as GameObject;// set the instantiate to be a gameobject
        shield.transform.parent = Boat.transform;//makes the shiled position = the boat
        shield.SetActive(false); //so it is disabled(you cant see it)

        forwardGUI.gameObject.SetActive(false);
        backGUI.gameObject.SetActive(false);
        notMovingGUI.gameObject.SetActive(true);

       
    }

    // Update is called once per frame
    void Update()
    {

        float forward = Input.GetAxis("forward");

        float rightaxis = Input.GetAxis("Vertical");


        //changing gears and delay
        if (buttonDelay > 10) //0.5sec before = true
        {
            if (Input.GetKey(Up) && gear < 3 || forward > 0 && gear < 3)
            {
                gear = gear + 1;
                buttonDelay = 0;
                Debug.Log("gear =" + gear);

            }

            if (Input.GetKey(Down) && gear > 1 || forward < 0 && gear > 1)
            {
                gear = gear - 1;
                buttonDelay = 0;
                Debug.Log("gear =" + gear);

            }

        }
        buttonDelay = buttonDelay + 1;


        if (gear == 1)//reverse
        {


            forwardGUI.gameObject.SetActive(false);
            backGUI.gameObject.SetActive(true);
            notMovingGUI.gameObject.SetActive(false);

            if (speed > 0f)//makes the boat stop moving foward so it looks like the shift of momentum is changing
            {
                speed = speed - 0.00025f;
                this.transform.position += this.transform.forward * this.speed;
            }

            if (reverse < 0.05f)//set a cap
            {
                reverse = reverse + 0.00025f;
            }
            this.transform.position -= this.transform.forward * this.reverse;
        }



        if (gear == 2) //drifting
        {

            forwardGUI.gameObject.SetActive(false);
            backGUI.gameObject.SetActive(false);
            notMovingGUI.gameObject.SetActive(true);

            if (speed > 0f)
            {
                speed = speed - 0.00025f;
                this.transform.position += this.transform.forward * this.speed; //allows the boat to drift to a slow
            }
            else if (reverse > 0f)
            {
                reverse = reverse - 0.00025f;
                this.transform.position -= this.transform.forward * this.reverse;
            }

        }

        if (gear == 3) //forward
        {

            forwardGUI.gameObject.SetActive(true);
            backGUI.gameObject.SetActive(false);
            notMovingGUI.gameObject.SetActive(false);

            if (reverse > 0f)
            {
                reverse = reverse - 0.00025f;
                this.transform.position -= this.transform.forward * this.reverse;
            }

            if (speed < 0.1f)//set a cap
            {
                speed = speed + 0.00025f;
            }
            this.transform.position += this.transform.forward * this.speed;
        }
    
       


        if (Input.GetKey(left) || rightaxis < 0)
        {
            this.transform.Rotate(Vector3.down * turnspeed * Time.deltaTime);
        }

        if (Input.GetKey(right) || rightaxis > 0)
        {
            this.transform.Rotate(Vector3.up * turnspeed * Time.deltaTime);
        }

        
        fireOn = HealthBarP2.fireOn;
        onSmoke = HealthBarP2.smokeOn;
        shieldHit = HealthBarP2.shieldHit;
       

        if (shieldOn == true)
        {
            shield.SetActive(true);
           if(shieldHit == true)
           {
               shieldOn = false;
               shieldHit = false;
               Debug.Log("p2 shieldOn and shield hit set to false");
           }
            
        }
        else
            {
                //Debug.Log("sheild set to false");
                shield.SetActive(false);
            }


       

        if (fireOn == true)
        {
            if (fireOnOff == 0)//this way it will only instantiate once
            {
                onFire = Instantiate(onFirePrefab, this.transform.position, this.transform.rotation) as GameObject;
                onFire.transform.parent = Boat.transform;
                fireOnOff = fireOnOff + 1;
            }
        }




        if (onSmoke == true)
        {
            if (smokeOnOff == 0)
            {
                smokeOn = Instantiate(smokeOnPrefab, this.transform.position, this.transform.rotation) as GameObject;
                smokeOn.transform.parent = Boat.transform;
                smokeOnOff = smokeOnOff + 1;
            }
        }


        if (nukeOnline == true)
        {
            nukeActive = true;
            //Debug.Log("nuke active=" + nukeActive);
        }
        else
        {
            nukeActive = false;
        }


        nukeOnline = P2NukeLocation.nukeActive;








    }


	IEnumerator SpeedUp()
    {
		speed = 0.4f;
		yield return new WaitForSeconds(5);
		speed = 0.1f;
	}



    void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("PowerUp hit Boat");

        if (collision.gameObject.tag == "PowerUpSpeed") //This is done so if this boat dectects it is hit by a bullet it will lose health. It has to be in this scrip coz its saves me not having to re type code
        {

            StartCoroutine(SpeedUp()); //I needed to use the coroutine in the and ienumerator for this to work correctly

        }

        if (collision.gameObject.tag == "PowerUpShield")
        {

            if (shieldOn == false)//this is to stop the shields stacking
            {
                shield.SetActive(true);
                shieldOn = true;
               // Debug.Log("p2 shieldOn @ shieldSetActive set to true");

            }

        }

        if (collision.gameObject.tag == "PowerUpNuke")
        {

            nukeOnline = true;
           
              //  Debug.Log("PowerUpNuke picked up");  

        }
    }

}


