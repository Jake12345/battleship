﻿using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour
{


    public float maxHealth = 100f;
    public float health = 0f;
    public GameObject healthbar;
    Collision collision;
    public GameObject destory;
    public static bool shieldOn;
    public static bool fireOn = false;
    public static bool smokeOn = false;
    public static bool shieldHit = false;


	// Use this for initialization
	void Start () 
    {
        health = maxHealth;
        
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        shieldOn = Player1.shieldOn;//finds out if the shield is on by looking at the shieldOn script
        shieldHit = Player1.shieldHit;

        //DecreaseHealth(); //test
        if (health < 0f)
        {
            Destroy(destory.gameObject);
            Application.LoadLevel(3);
        }

        if (health < 50)
        {
            fireOn = true;
            
        }

        if (health < 70)
        {
            smokeOn = true;
        }

        Debug.Log("shield on in healthbar = " + shieldOn);
	}

    void DecreaseHealth()//Bullets
    {
        health = health - 16;
        float currentHealth = health / maxHealth;
        SetHealthBar(currentHealth);
    }

    void DecreaseHealth2()//Torpedo
    {
        health = health - 8;
        float currentHealth = health / maxHealth;
        SetHealthBar(currentHealth);
    }

    void DecreaseHealth3()//Nuke
    {
        health = health - 60;
        float currentHealth = health / maxHealth;
        SetHealthBar(currentHealth);
    }

    
    public void SetHealthBar(float myHealth)
    {
        healthbar.transform.localScale = new Vector3(Mathf.Clamp(myHealth, 0f, 1f), healthbar.transform.localScale.y, healthbar.transform.localScale.z);//This is what lowers the health bar
    }

    void OnTriggerEnter(Collider other)
    {
        if (shieldOn == false)
        {
            Debug.Log("Bullet hit Boat");
            DecreaseHealth();
        }

        if (shieldOn == true)
        {
            shieldHit = true;
            Debug.Log("Shield hit by missle - shieldhit == true");
        }

        if (other.tag == "NukeExplosion")
        {
            DecreaseHealth3();
            Debug.Log("Nuke hit Boat");
        }

    }

    void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Torpedo" && shieldOn == false) //will only take damage if the shield is offline
        {
            Debug.Log("Torpedo hit Boat");
            DecreaseHealth2();
        }
        else if (collision.gameObject.tag == "Torpedo" && shieldOn == true)
        {
            Debug.Log("Shield been hit by Torpedo - shield hit = false");
            shieldHit = true;

        }



    }
}
