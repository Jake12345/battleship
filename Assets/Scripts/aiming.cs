﻿using UnityEngine;
using System.Collections;

public class aiming : MonoBehaviour {
    public KeyCode forward;
    public KeyCode back;
    public KeyCode left;
    public KeyCode right;
    public float speed = 0.2f;
    public KeyCode alt;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(forward) && Input.GetKey(alt))
        {
            this.transform.position += this.transform.forward * this.speed;
        }

        if (Input.GetKey(back) && Input.GetKey(alt))
        {
            this.transform.position -= this.transform.forward * this.speed;
        }

        if (Input.GetKey(left) && Input.GetKey(alt))
        {
            this.transform.position += this.transform.right * this.speed;
        }

        if (Input.GetKey(right) && Input.GetKey(alt))
        {
            this.transform.position -= this.transform.right * this.speed;
        }

    

    }
}
