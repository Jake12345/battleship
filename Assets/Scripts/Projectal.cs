﻿using UnityEngine;
using System.Collections;

public class Projectal : MonoBehaviour {


    private Transform m_transform;
    private Transform[] m_bunchOfDots; //array
    public GameObject Dot;
    public float force = 10;
    private Vector3 m_direction; //aka angle 
    public Camera mainCamera;
    public float gravity = 9.8f;
    private Rigidbody m_rigidbody;
	public KeyCode shoot;

	// Use this for initialization
	void Start () 
    {
        m_transform = transform;

        m_rigidbody = GetComponent<Rigidbody>();

        m_bunchOfDots = new Transform[50];//stating the max about 

        for (int i = 0; i < m_bunchOfDots.Length; i++) //.length is used so the maxS number of the array is used
        {
            GameObject tempObject = (GameObject)Instantiate(Dot);

            m_bunchOfDots[i] = tempObject.transform;

            m_bunchOfDots[i].gameObject.SetActive(false);
        }

  


	}
	
	// Update is called once per frame
	void Update ()
    {

        if (Input.GetKey(shoot))
        {
            Vector3 screenPos = mainCamera.WorldToScreenPoint(m_transform.position); //creates scale on what the camera can see so every pixel on the screen can be like 100,300 or something.

            screenPos.z = 0;

            //get two points to creat the vector angle. we then normalized it so its distance becomes 1 but we keep the angle. this is so the object speed is not the vectors distance the force timesed together. 


            m_direction = (screenPos - Input.mousePosition).normalized; //the mouse position on the screen - screenPos creates a angle that connects the two points. thi is th angle used for the direction.think angle birds catapult using finger.

            Aim();
        }
        if(Input.GetKeyUp(shoot))//remove dots is i let go on the mouse button
        {
            for (int i = 0; i < m_bunchOfDots.Length; i++) //.length is used so the maxS number of the array is used
            {
                m_bunchOfDots[i].gameObject.SetActive(false);
            }
            m_rigidbody.velocity = m_direction * force;//force that pushed the object!!!!!!!!!!!!!!!
        }

	}


    private void Aim()
    {

        float SX = m_direction.x * force; //direction times force on the x axis
        float sY = m_direction.y * force;

          for(int i = 0; i< m_bunchOfDots.Length; i++)
        {
            float t = i * 0.1f;

            float dX = SX * t;

            float dY = (sY * t) - (0.5f * gravity * (t * t));
            m_bunchOfDots[i].position = new Vector3(m_transform.position.x + dX, m_transform.position.y +dY,0.0f);

            m_bunchOfDots[i].gameObject.SetActive(true); //so only when i click on the screen the directions balls will appear 
        }

    }
}
