﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour
{

    public KeyCode forward;
    public KeyCode back;
    public KeyCode left;
    public KeyCode right;
    public float speed = 0.2f;
    public float turnspeed = 2.0f;

    // Use this for initialization
    void Start()
    {
     


    }

    // Update is called once per frame
    void Update()
    {


        if (Input.GetKey(forward))
        {
            this.transform.position += this.transform.forward * this.speed;
        }

        if (Input.GetKey(back))
        {
            this.transform.position -= this.transform.forward * this.speed;
        }

        if (Input.GetKey(left))
        {
            this.transform.Rotate(0, -this.turnspeed, 0);
        }

        if (Input.GetKey(right))
        {
            this.transform.Rotate(0, this.turnspeed, 0);
        }

    
    }
}