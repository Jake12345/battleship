﻿using UnityEngine;
using System.Collections;

public class Torpedo : MonoBehaviour {

	private Rigidbody m_rigidbody;
	private Transform m_transform;
	public float force = 100.0f;
	Collision collision;
	public float timer = 5.0f;
    private GameObject explosion;
    public GameObject explosionPrefab;

	
	// Use this for initialization
	void Start()
	{
		m_rigidbody = GetComponent<Rigidbody>();
		m_transform = GetComponent<Transform>();
		
		m_rigidbody.AddForce(m_transform.forward * force, ForceMode.Impulse);
	}
	
	// Update is called once per frame
	void Update()
	{
		Object.Destroy(this.gameObject,timer);

	}
	
	void OnCollisionEnter(Collision collision)
	{
	if (collision.gameObject.tag == "P2Boat" ) //once it hits the boat it destorys itself
		{
			Destroy(this.gameObject);
            explosion = Instantiate(explosionPrefab, this.transform.position, this.transform.rotation) as GameObject;
		}
    if (collision.gameObject.tag == "P1Boat") //once it hits the boat it destorys itself
    {
        Destroy(this.gameObject);
        explosion = Instantiate(explosionPrefab, this.transform.position, this.transform.rotation) as GameObject;
    }

	}
}
