﻿using UnityEngine;
using System.Collections;

public class BulletP2 : MonoBehaviour {

    public float speed = 1.0F;
    private float startTime;
    private float journeyLength;
    private static Vector3 gun;
    private static Vector3 target;
    private GameObject explosion;
    public GameObject explosionPrefab;


    // Use this for initialization
    void Start()
    {
        target = TargetLocationP2.location;
        gun = GunPositionP2.location;
        startTime = Time.time;


        journeyLength = Vector3.Distance(gun, target);
    }

    // Update is called once per frame
    void Update()
    {
        float distCovered = (Time.time - startTime) * speed;
        float fracJourney = distCovered / journeyLength;
        transform.position = Vector3.Lerp(gun, target, fracJourney);
    }


    void OnCollisionEnter(Collision collision)
    {
        

        if (collision.gameObject.layer == LayerMask.NameToLayer("Water")) //this this what it colled with
        {
            Debug.Log("hit sea");
            Destroy(this.gameObject); //so destory itself. !!! add explosion stuff above this later
            explosion = Instantiate(explosionPrefab, this.transform.position, this.transform.rotation) as GameObject;// set the instantiate to be a gameobject
            //Destroy(explosion.gameObject); //the explosions prefabs are not being deleted once they have been used. This dose not work.needs delay?
        }






    }
}
