﻿using UnityEngine;
using System.Collections;

public class P1NukeLocation : MonoBehaviour {

    public static bool nukeActive;
    public KeyCode NukeKey;
    private GameObject Nuke;
    public GameObject NukePrefab;
    public static bool nukeUsed;



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        nukeActive = Player1.nukeActive;
        //Debug.Log("nuke active at location =" + nukeActive);

        if (Input.GetKey(NukeKey))
        {
            Debug.Log("N key pressed");
            if (nukeActive == true)
            {
                Debug.Log("Nuke launched");
                Nuke = Instantiate(NukePrefab, this.transform.position, this.transform.rotation) as GameObject;
                nukeActive = false;//so it only spawns one
            }
        }


    }
}
