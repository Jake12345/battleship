﻿using UnityEngine;
using System.Collections;

public class BulletP1 : MonoBehaviour {

 
    public float speed = 1.0F;
    private float startTime;
    private float journeyLength;
    private static Vector3 gun;
    private static Vector3 target;
    private GameObject explosion;
    public GameObject explosionPrefab;
    public static float boatSpeed;

    // Use this for initialization
    void Start()
    {
        target = TargetLocationP1.location;
        gun = GunPositionP1.location;
        startTime = Time.time;

        
        journeyLength = Vector3.Distance(gun, target);
    }

    // Update is called once per frame
    void Update()
    {

        float distCovered = (Time.time - startTime) * speed;
        float fracJourney = distCovered / journeyLength;
        transform.position = Vector3.Lerp(gun, target, fracJourney + boatSpeed);
    }
      

    void OnCollisionEnter(Collision collision)
    {
        

        Debug.Log("hit sea");
        Destroy(this.gameObject); //so destory itself. !!! add explosion stuff above this later
        explosion = Instantiate(explosionPrefab, this.transform.position, this.transform.rotation) as GameObject;
        
       
    }

  

}
