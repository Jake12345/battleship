﻿using UnityEngine;
using System.Collections;

public class P2Bullet : MonoBehaviour {

    private Rigidbody m_rigidbody;
    private Transform m_transform;
    public float force = 100.0f;
    Collision collision;
    Vector3 aim;
    public float speed;
    Vector3 player1;
    Vector3 differentDifference;

    // Use this for initialization
    void Start()
    {
        m_rigidbody = GetComponent<Rigidbody>();
        m_transform = GetComponent<Transform>();
        aim = GameObject.Find("P2Target").transform.position;
        player1 = GameObject.Find("P2Boat").transform.position;
        m_rigidbody.AddForce(m_transform.forward * force, ForceMode.Impulse);


    }

    // Update is called once per frame
    void Update()
    {

        transform.position = Vector3.Lerp(aim, player1, Mathf.PingPong(Time.time * speed, 0.25f));
    }

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("hit sea");

        if (collision.gameObject.tag == "Terrain") //this this what it colled with
        {
            Destroy(this.gameObject); //so destory itself. !!! add explosion stuff above this later
        }






    }
}
