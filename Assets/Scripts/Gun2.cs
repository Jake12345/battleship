﻿using UnityEngine;
using System.Collections;

public class Gun2 : MonoBehaviour {

    private Transform m_transformer;
    public GameObject bulletPrefab;
    public KeyCode shoot;
    private int bulletDelay = 0;
    // Use this for initialization
    void Start()
    {
        m_transformer = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {

        if (bulletDelay > 80)
        {
            if (Input.GetKeyDown(shoot))
            {
                Instantiate(bulletPrefab, m_transformer.position + m_transformer.forward * 3.0f, m_transformer.rotation);
                bulletDelay = 0;
            }

        }
        else
        {
            bulletDelay = bulletDelay + 1;
        }
       


        //Debug.Log("bullet delay =" + bulletDelay);
    }
}