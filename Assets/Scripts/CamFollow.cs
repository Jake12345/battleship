﻿using UnityEngine;
using System.Collections;

public class CamFollow : MonoBehaviour 
{
	public Vector3 offsetVector = new Vector3(0.0f, 3.0f, -4.0f);
	public Transform objToFollow;

    public KeyCode left;
    public KeyCode right;
    public float turnspeed = 2.0f;

	private Transform camTransform;

	// Use this for initialization
	void Start () 
	{
		camTransform = transform;
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 wantedPosition = objToFollow.localPosition + offsetVector;
		camTransform.position = Vector3.Slerp (wantedPosition, camTransform.position, 0.94f);
		camTransform.LookAt (objToFollow.position);

        if (Input.GetKey(left))
        {
            this.transform.Rotate(0, -this.turnspeed, 0);
        }

        if (Input.GetKey(right))
        {
            this.transform.Rotate(0, this.turnspeed, 0);
        }
	}
}
