﻿using UnityEngine;
using System.Collections;

public class CameraFollowV2 : MonoBehaviour {

    public GameObject player;
    private bool ready;
    private Vector3 cameraLocation;
    public float height;

	// Use this for initialization
	void Start () 
    {
        height = 10f;
        transform.position= new Vector3(player.transform.position.x,player.transform.position.y+height,player.transform.position.z)+(-20)*player.transform.forward;

	}
	
	// Update is called once per frame
	void Update () 
    {

        cameraLocation = new Vector3(player.transform.position.x, player.transform.position.y + height, player.transform.position.z) + (-20) * player.transform.forward;

        if (!ready)
        {
            StartCoroutine("cam_rotate");
        }
        transform.LookAt(player.transform.position);
	}

    IEnumerator cam_rotate()
    {
        ready = true;
        transform.position = Vector3.Slerp(transform.position, cameraLocation, 3 * Time.deltaTime);
        ready = false;
        yield return new WaitForSeconds(0.005f);
    }

}
